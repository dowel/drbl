#!/bin/bash
# KVFGLWLAN WLAN for teachers
# KVFGSWLAN WLAN for pupils
# based on VLAN in local KvFG network environment


# fill the variables we need with useful stuff

#KVFGMAC=$(/usr/bin/cat /sys/class/net/wlp3s0/address)
KVFGUSER=$(zenity --entry --title "KvFG Username" --text "Username for KvFG-Network: ") ;
KVFGPASS=$(zenity --entry --hide-text --title "KvFG Password" --text "Password for KvFG-Network: ") ;

# L oder S / T or P

case ${#KVFGUSER} in

## Lehrer/innen (teachers) have only 3 or 4 letters in their username

        3|4) 
        KVFGLWLAN=$(cat  <<EOF
[connection]
id=lehrer
uuid=3f2d6f6e-8d41-42e9-a9b8-55b8de21e721
type=wifi
permissions=

[wifi]
mac-address-blacklist=
mode=infrastructure
ssid=lehrer

[wifi-security]
auth-alg=open
key-mgmt=wpa-eap

[802-1x]
anonymous-identity=$KVFGUSER
eap=peap;
identity=$KVFGUSER
password=$KVFGPASS
phase2-auth=mschapv2

[ipv4]
dns-search=
method=auto

[ipv6]
addr-gen-mode=stable-privacy
dns-search=
method=auto

EOF
)

# print to filesystem
echo "$KVFGLWLAN" > /home/kvfg/lehrer.nmconnection

# move to right place
SUDOPASS=$(zenity --entry --hide-text --title "Password for local user kvfg" --text "Enter the password for the local user kvfg: ")
echo -e "$SUDOPASS\n" | sudo -S -s -- mv /home/kvfg/lehrer.nmconnection /etc/NetworkManager/system-connections/
sudo -s -- chown root:root /etc/NetworkManager/system-connections/lehrer.nmconnection
sudo -s -- chmod 600 /etc/NetworkManager/system-connections/lehrer.nmconnection
sudo -s -- systemctl restart NetworkManager
;;

## Schüler/innen (pupils) have more than 3 letters in their username

        *)
        KVFGSWLAN=$(cat  <<EOF
[connection]
id=schueler
uuid=3f2d6f6e-8d41-42e9-a9b8-55b8de21e721
type=wifi
permissions=

[wifi]
mac-address-blacklist=
mode=infrastructure
ssid=schueler

[wifi-security]
auth-alg=open
key-mgmt=wpa-eap

[802-1x]
anonymous-identity=$KVFGUSER
eap=peap;
identity=$KVFGUSER
password=$KVFGPASS
phase2-auth=mschapv2

[ipv4]
dns-search=
method=auto

[ipv6]
addr-gen-mode=stable-privacy
dns-search=
method=auto

EOF
)

# print to filesystem
echo "$KVFGSWLAN" > /home/kvfg/schueler.nmconnection

# move to right place
SUDOPASS=$(zenity --entry --hide-text --title "Password for local user kvfg" --text "Enter the password for the local user kvfg: ")
echo -e "$SUDOPASS\n" | sudo -S -s -- mv /home/kvfg/schueler.nmconnection /etc/NetworkManager/system-connections/
sudo -s -- chown root:root /etc/NetworkManager/system-connections/schueler.nmconnection
sudo -s -- chmod 600 /etc/NetworkManager/system-connections/schueler.nmconnection
sudo -s -- systemctl restart NetworkManager
;;

esac

# Check environment

if [ ! -d "/home/kvfg/ServerG-Homeverzeichnis" ]
then
        sudo -s -- mkdir /home/kvfg/ServerG-Homeverzeichnis
        sudo -s -- chown -R kvfg.kvfg /home/kvfg/ServerG-Homeverzeichnis
fi

if [ ! -d "/home/kvfg/ServerG-Tauschverzeichnis" ]
then
        sudo -s -- mkdir /home/kvfg/ServerG-Tauschverzeichnis
        sudo -s -- chown -R kvfg.kvfg /home/kvfg/ServerG-Tauschverzeichnis
fi

# Inform user

(
echo "10" ; sleep 3
echo "# Restarting NetworkManager" ; sleep 3
echo "20" ; sleep 3
echo "# Is it up?" ; sleep 3
echo "50" ; sleep 3
echo "# Let me try to mount shares from ServerG" ; sleep 3
echo "75" ; sleep 3
echo "# If it did not work - check network and repeat ..." ; sleep 3
echo "100" ; sleep 3
) |
zenity --progress \
  --title="Waiting for KvFG Connection" \
  --text="Connecting to ServerG environment ..." \
  --percentage=0

# Mount ServerG Shares to user home
sudo -s -- mount -v -t cifs -o user=$KVFGUSER,password=$KVFGPASS,rw,iocharset=utf8,vers=2.0 //10.16.1.1/$KVFGUSER /home/kvfg/ServerG-Homeverzeichnis
sudo -s -- mount -v -t cifs -o user=$KVFGUSER,password=$KVFGPASS,rw,iocharset=utf8,vers=2.0 //10.16.1.1/Tausch /home/kvfg/ServerG-Tauschverzeichnis

# Deleting credentials
SUDOPASS="";
sudo -K

## We have to delete nmconnection files on shutdown and reboot 
## And we should unmount ServerG shares because we are friendly
## See service file