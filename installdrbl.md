# Installation DRBL Server / Clonezilla Server

Quellen:

  * https://drbl.org/installation/02-install-required-packages.php
  * https://dwaves.de/wp-content/uploads/2014/10/Setup-a-Clonezilla-Server-on-Debian-Ubuntu.pdf
  * https://brezular.com/2016/09/26/clonezilla-server-edition-installation-on-ubuntu/

## Ips factum on Debian 11

### Basic


````
wget -q http://drbl.org/GPG-KEY-DRBL -O- | sudo apt-key add -
````

add to /etc/apt/sources.list

````
deb http://ftp.us.debian.org/debian/ bullseye main # (Or any Debian mirror site near you)
deb http://free.nchc.org.tw/drbl-core drbl stable

````

Install DRBL

````
apt update
apt install drbl

````

Check IP Addresses

````
ip a s
````

Leave one to NetworkManager (WAN). Set the other one as a static one via /etc/network/interfaces (LAN - for cloning)

````
auto enp0s8
  iface enp0s8 inet static
  address 192.168.0.1
  netmask 255.255.0.0

````

### Installation DRBL

````

root@drbl:~# drblsrv -i
*****************************************************.
Hinweis! Wenn eine "Ja/Nein-Auswahl" (Yes/No) möglich ist, ist der Standardwert in Großbuchstaben. Z.B.ist bei (y/N) der Standardwert "N", wenn Sie also "Enter" tippen ohne "Y" oder "N" wird Ihre Eingabe wie "N" und "Enter" behandelt. Wenn Sie sich nicht sicher sind, drücken Sie also einfach die "Enter"-Taste.
*****************************************************.
*****************************************************.
Installiere DRBL für Debian Linux...
*****************************************************.
Wollen Sie die Netzwerk-Boot-Images für die Installation verwenden, so dass einige GNU/Linux Distributionen vom Client-Computer übers Netz installiert weren können (z.B. Debian, Ubuntu, RedHat Linux, Fedora Core, Mandriva, CentOS and OpenSuSE...)?  !!HINWEIS!! Dies lädt eine Menge Dateien herunter (typischerweise > 100 MB) und benötigt daher einige Zeit. Wenn der Client-Computer eine Festplatte hat, auf der Sie GNU/Linux installieren können, geben Sie Y ein. Wenn Sie  "no" antworten, können Sie die Installation später mit "drbl-netinstall" nachholen.
[y/N] N

*****************************************************.
This GNU/Linux distribution uses one kernel to support SMP and non-SMP arch.
*****************************************************.
Wollen Sie die Ausgabe über die serielle Schnittstelle der Client-Rechner verwenden?
Wenn Sie NICHT wissen, was Sie eingeben sollen, geben Sie "N" ein, da sonst die Client-Computer möglicherweise keine Bildschirmausgabe anzeigen!
[y/N] N

The CPU arch option for your clients: 2
Die Optimierung für Ihr System wird entsprechend diesem Server eingestellt.
*****************************************************.
Leere den APT-Cache damit einige Einstellungen übernommen werden...
OK:1 http://deb.debian.org/debian bullseye InRelease
OK:2 http://deb.debian.org/debian bullseye-updates InRelease
OK:3 http://security.debian.org/debian-security bullseye-security InRelease
OK:4 http://ftp.us.debian.org/debian bullseye InRelease     
OK:5 http://free.nchc.org.tw/drbl-core drbl InRelease       
Paketlisten werden gelesen… Fertig
*****************************************************.
Wollen Sie das Betriebssystem aktualisieren?
[y/N] N

````

Installer installs things ... grab a cup of tea.

````

*****************************************************.
Versuch des Upgrades einiger notwendigen Packages, falls diese verfügbar sind...
*****************************************************.
Im ayo-Repository...suche das neueste  kernel ...
Der neueste Kernel im ayo-Repository ist linux-image-5.10.0-10-amd64
Es sind 2 Kernels für die Clients vorhanden, welchen ziehen Sie vor?
[1]: kernel 5.10.0-10-amd64 x86_64 (von diesem DRBL-Server)
[2]: linux-image-5.10.0-10-amd64 (aus dem  APT-Repository)
[1] 1

````

Installer creates config


````
Finished!
Fertig!
*****************************************************.
Fertig!
````

### Second step

Interruptions only if not using defaults:

````
root@drbl:/etc/drbl# drblpush -i
******************************************************
Bemerkung! Wenn eine "Ja/Nein-Auswahl" (Yes/No) möglich ist, ist der Standardwert in Großbuchstaben. Z.B.ist bei (y/N) der Standardwert "N", wenn Sie also "Enter" tippen ohne "Y" oder "N" wird Ihre Eingabe wie "N" und "Enter" behandelt. Wenn Sie sich nicht sicher sind, drücken Sie also einfach die "Enter"-Taste.
******************************************************
Suche installierte Packages für den DRBL-Server...Dies kann einige Minuten dauern...
Suche nach installierten Package für den DRBL-Server beendet.
******************************************************
------------------------------------------------------
Im interaktiven Mode können Sie die Informationen zu Ihrer DRBL-Umgebung eingeben.
------------------------------------------------------
------------------------------------------------------
Geben Sie jetzt die DNS-Domain (wie z.B. drbl.sf.net) an:
[drbl.org] 
Setze DOMAIN auf drbl.org
------------------------------------------------------
Geben Sie den NIS/YP Domain-Namen an:
[penguinzilla] 
Setze DOMAIN auf penguinzilla
------------------------------------------------------
Geben Sie den Präfix des Client-Hostnamen an:
Dieser Präfix wird verwendet, um automatisch Hostnamen für Clients zu erzeugen. Wenn Sie einige oder alle automatisch erstellten Hostnamen übersteuern möchten, drücken Sie jetzt Ctrl-C, um das Programm zu beenden, anschließend editieren Sie /etc/drbl/client-ip-hostname und starten dann dieses Programm erneut.
[drbl-] 
Setze den Hostname-Präfix für Clients auf drbl-
------------------------------------------------------
enp0s3: IP address 10.16.20.13, netmask 255.255.0.0
enp0s8: IP address 192.168.0.1, netmask 255.255.0.0
In Ihrem System sind als Netzwerk-Karten konfiguriert: enp0s3 enp0s8 
------------------------------------------------------
Die öffentliche IP-Adresse dieses Servers ist NICHT vorhanden.
Welcher Ethernet-Port auf diesem Server ist für den Internet-Zugang, nicht für DRBL-Verbindungen?
Verfügbare Ethernet-Ports auf diesem Server:
enp0s3 (10.16.20.13), enp0s8 (192.168.0.1), 
[enp0s3]
````
Check the above for your environment!

````
Der Ethernet-Port für die WAN-Verbindung ist: enp0s3
Die Ethernet-Ports für die DRBL-Umgebung:  enp0s8 
******************************************************
Wir sammeln jetzt die MAC-Adressen der Clients!
Wenn Sie wollen, dass der DHCP-Service auf dem DRBL-Server immer die selbe Adresse an einen Client vergibt, wenn dieser bootet, sollten Sie dies jetzt angehen, falls Sie es nicht bereits früher getan haben!
Wenn Sie bereits die MAC-Adressen der Clients kennen, können Sie diese in verschiedenen 'Gruppendateien' eintragen (die Anzahl dieser Dateien entspricht der Anzahl der Netzwerk-Karten für den DRBL-Service). In diesem Fall können Sie diesen Schritt übergehen.
In diesem Schritt sammeln Sie die MAC-Adressen der Clients und teilen sie in verschiedene Gruppen auf. Das spart Zeit und verhindert Tippfehler.
Die MAC-Adressen werden Zug um Zug mit dem Start der Clients gesammelt,
und sie werden in unterschiedliche Dateien entsprechend der Netzwerk-Karte auf dem Server abgelegt, die Dateinamen entsprechen dabei macadr-eth1.txt, macadr-eth2.txt... Sie finden die Dateien in /etc/drbl.
Starten Sie nun nacheinander die Clients und stellen Sie sicher, dass sie über etherboot oder PXE booten!
Wollen Sie sie sammeln?
[y/N] 
******************************************************
OK! Weiter gehts...
******************************************************
Hostmin: 192.168.0.1
Wenn Sie wollen, dass der DHCP-Service auf dem DRBL-Server immer die selbe Adresse an einen Client vergibt, wenn dieser bootet (wenn Sie diese Funktion benötigen, müssen Sie die MAC-Adressen der Clients sammeln und in Dateien speicher (wie im vorangegangenen Schritt)). Es geht um die Clients, die mit der Ethernet-Schnittstelle des Servers verbunden sind enp0s8 ?
[y/N] 
******************************************************
OK! Machen wir weiter, wir vergeben die IP-Adressen der Clients nach dem Prinzip "first to boot gets first IP" anstelle von festen Werten!
******************************************************
Welches ist der erste Wert für den letzten Ziffernblock in der IP-Adresse (also der Startwert für d in der IP-Adresse a.b.c.d) für die DRBL-Clients an diesem Ethernet-Port enp0s8.
[1] 10
******************************************************
Wieviele DRBL-Clients (PC für Studenten) sind mit der Netzwerk-Schnittstelle des DRBL-Servers verbunden enp0s8 ?
Geben Sie die Anzahl ein: 
[12] 200
````

This depends on ... well .. how many clients you have.

````
******************************************************
Wir setzen die IP-Adresse für die Clients, die an die Ethernet-Schnittstelle des DRBL-Servers angeschlossen sind enp0s8 als: 192.168.0.10 - 192.168.0.209
Akzeptieren ? [Y/n] 
******************************************************
OK! Weiter gehts...
******************************************************
Das Layout für Ihre DRBL-Umgebung: 
******************************************************
          NIC    NIC IP                    Clients
+-----------------------------+
|         DRBL SERVER         |
|                             |
|    +-- [enp0s3] 10.16.20.13 +- to WAN
|                             |
|    +-- [enp0s8] 192.168.0.1 +- to clients group enp0s8 [ 200 clients, their IP 
|                             |            from 192.168.0.10 - 192.168.0.209]
+-----------------------------+
******************************************************
Total clients: 200
******************************************************
Weiter mit 'Enter'... 
******************************************************
------------------------------------------------------
Auf dem System gibt es 3 Arten für Diskless Linux-Services:
[0] Voller DRBL-Mode, jeder Client hat sein eigenes, NFS-basiertes /etc and /var.
[1] DRBL SSI (Single system image) Mode, jeder Client hat /etc und /var im tmpfs. In diesem Mode wird die Belastung und der benötigte Plattenplatz auf dem Server geringer sein. HINWEIS! In diesem Mode (1) benötigt der Client mindestens 256 MB Hauptspeicher. (2) werden die Einstellungen und Konfigurationsdateien nicht auf den DRBL-Server gesichert! Sie werden nur einmal genutzt und verschwinden beim Abschalten des Systems! Außerdem müssen Sie, wenn Sie eine Datei der Client-Vorlagen (abgelegt in /tftpboot/nodes) ändern, erneut ds Script drbl-gen-ssi-files aufrufen, um das Vorlagen-Archiv in /tftpboot/node_root/drbl_ssi/ zu aktualisieren. (3) Wenn Sie Änderungen im Vorlagen-Archiv über eine Datei machen wollen, lesen Sie /tftpboot/node_root/drbl_ssi/clients/00_README um Näheres zu erfahren.
[2] Ich möchte keine Diskless Linux-Services für Clients verfügbar machen.
Welchen Mode wollen Sie?
[0] 1
````
Is lighter on the server.

````
DRBL SSI mode ist eingestellt, eine elegante DRBL-Umgebung ist in Arbeit!
******************************************************
------------------------------------------------------
Auf dem System gibt es 4 Arten zur Nutzung von Clonezilla:
[0] Voller Clonezilla-Mode, jeder Client hat sein eigenes, NFS-basiertes /etc and /var.
[1] Gekapselter Clonezilla-Mode, jeder Client hat /etc und /var im tmpfs. In diesem Mode wird die Belastung und der benötigte Plattenplatz auf dem Server geringer sein. Hinweis! Im gekapselten Clonezilla-Mode werden die Einstellungen und Konfigurationsdateien nicht auf den DRBL-Server gesichert! Sie werden nur einmal genutzt und verschwinden beim Abschalten des Systems! 
[2] Ich möchte kein Clonezilla.
[3] Clonezilla Live als Betriebssystem für die Clients verwenden.
Welchen Mode wollen Sie?
[0] 1
````
Is lighter on the server.

````
Der gekapselte Clonezilla-Mode ist eingestellt, eine elegante Clonezilla-Umgebung ist in Arbeit!
******************************************************
******************************************************
Die CPU-Architektur für Clonezilla ist eingestellt auf : amd64
------------------------------------------------------
In welchem Serververzeichnis sollen die gesicherten Images für die Benutzung von Clonezilla abgelegt werden (bitte absoluten Pfad angeben, es darf allerdings NICHT unter /mnt/, /media/ or /tmp/ sein)?
[/home/partimag] 
Directory for Clonezilla saved images: /home/partimag
------------------------------------------------------
Wenn auf der lokalen Festplatte eine Swap-Partition existiert oder ein beschreibbares Filesystem vorhanden ist,
wollen Sie diese Swap-Partiton verwenden oder eine Swapdatei in dem beschreibbaren Filesystem erstellen, damit der Client mehr Speicher zur Verfügung hat? (Dieser Schritt zerstört KEINE Daten auf der Festplatte)
[Y/n] 
******************************************************
OK! Wir erstellen einen Swapspace für Ihren Client, wenn eine lokale Festplatte verfügbar ist!
------------------------------------------------------
Wieviel Platz (Megabytes) soll maximal für den Swapspace verwendet werden?
Wir versuchen den Swapspace für Sie anzulegen, wenn es nicht reicht, werden 60% des freien Speichers verwendet
[128] 512
````
Well ... HDD and RAM is cheap.

````
******************************************************
------------------------------------------------------
Welchen Mode sollen die Clients nach dem Booten verwenden?
"1": Grafikmode (X window system) (Standard),
"2": Textmode.
[1] 
Die Clients werden nach dem Booten im Grafikmode starten.
******************************************************
------------------------------------------------------
In welchem Mode soll die grafische Oberfläche starten?
0: Normales Login, 1: Autologin, 2: zeitgesteuertes Login
[0] 
Die Clients warten auf einen Benutzerlogin, wenn Sie starten.
******************************************************
------------------------------------------------------
Wollen Sie für jeden Client ein root-Passwort angeben, anstatt dasselbe Passwort vom Server zu übernehmen? (Sicherheitsgewinn)
[y/N] 
OK! Weiter gehts...
------------------------------------------------------
Wollen Sie das pxelinux-Passwort für Clients setzen, so dass vor dem Booten des Clients ein Passwort eingegeben werden muss (Sicherheitsgewinn)
[y/N] 
OK! Weiter gehts...
------------------------------------------------------
Wollen Sie den Boot-Prompt für die Clients setzen?
[Y/n] 
Wie lange (in 1/10 Sek.) soll vor dem Booten der Clients gewartet werden?
[70] 
OK! Weiter gehts...
------------------------------------------------------
------------------------------------------------------
Wollen Sie das PXE-Menü mit grafischem Hintergrund für den Start der Clients?
Hinweis! Wenn Sie mit grafischem PXELinux-Menü booten und der Bootvorgang fehlschlägt, können Sie durch Aufruf von "switch-pxe-bg-mode -m text" in den Textmode umschalten.
[Y/n] 
Use graphic PXE Linux menu for the client.
------------------------------------------------------
------------------------------------------------------
Wollen Sie, dass Audio, CDRom, Floppy, Video und Plugdev (wie USB-Geräte) für alle Benutzer des DRBL-Clients nutzbar sein sollen? Falls ja, werden wir alle Benutzer in diese Gerätegruppen auf Server und Clients eintragen.
[Y/n] 
OK! Weiter gehts...
------------------------------------------------------
------------------------------------------------------
Mit einem Alias-Interface kann jeder Client 2 IPs haben,
eine ist eine private IP für die Verbindung zum DRBL-Server, die andere die öffentliche IP fürs WAN!
Wollen Sie eine öffentliche IP für die Clients konfigurieren?
[y/N] 
------------------------------------------------------
Wollen Sie, dass die DRBL-Clients optional im Terminalmode laufen können? Wenn Sie wollen, dass der Client seine Anzeige remote verfügbar machen kann (was hauptsächlich Server-Resourcen benötigt), geben Sie  hier "Y" ein.
Hinweis!
0. Wenn Sie mit ja antworten, wird nur eine sehr eingeschränkte Umgebung auf dem Client verfügbar sein, z.B. KEINE USB-Geräte, CD, Audio, Drucker, etc. im Client.
1. Wenn Ihr Server nicht leistungsfähig ist, geben Sie hier "no" ein.
2. Mit "yes" als Eingabe, werden wir xdmcp aktivieren,
Das ist niemals eine sichere Sache.  Die Konfiguration von /etc/hosts.allow und /etc/hosts.deny zur Einschränkung auf lokalen Zugriff ist eine Alternative, aber auch nicht besonders sicher.
Port 177 über Firewall-Regeln zu kontrollieren ist das Sicherste, wenn Sie xdmcp aktiviert haben.
Lesen Sie im Manual die weiteren Hinweise zur Sicherheit von XDMCP.
Machen Sie dies bitte selbst!
3. Wenn Sie hier "yes" antworten, kann es sein, dass Sie Ihre Desktop Displaymanager (gdm/kdm) später starten müssen, denken Sie daran, Ihre Daten zu sichern, bevor Sie die Anwendungen schließen!
Wollen Sie die Option auf den Terminal-Mode für die Clients aktivieren?
[y/N] 
OK! Weiter gehts...
------------------------------------------------------
------------------------------------------------------
Wollen Sie den DRBL-Server als NAT-System verwenden? Wenn nein, werden die DRBL-Clients NICHT aufs Internet zugreifen können.
[Y/n] 
OK! Weiter gehts...
------------------------------------------------------
******************************************************
Der aktuelle Kernel auf dem Server unterstützt NFS over TCP!
Hinweis! Wenn Sie den aktuellen Kernel des Servers ändern, und nicht sicher sind, ob der Kernel NFS Über UDP oder TCP, sollten Sie besser nochmal "drblpush -i" aufrufen um spätere Bootfehler der Clients zu verhindern!
Weiter mit 'Enter'... 
------------------------------------------------------
******************************************************
The calculated NETWORK for enp0s8 is 192.168.0.0.
******************************************************
******************************************************
Wir können jetzt die Dateien auf das System ausliefern! 
Wollen Sie fortsetzen?
Warnung! Wenn Sie weitermachen, werden Ihre Firewall-Regeln während des Setup überschrieben!
Die Originalregeln werden als iptables.drblsave im Systemkonfigurations-Verzeichnis (/etc/sysconfig or /etc/default) gesichert.
[Y/n] 
[Y/n] 
******************************************************
OK! Los gehts!
````

Things happen.

````
Enjoy DRBL!!!
http://drbl.org; http://drbl.nchc.org.tw
NCHC Free Software Labs, Taiwan. http://free.nchc.org.tw
*****************************************************.
Wenn Sie wollen, können Sie den DRBL-Server jetzt neu starten und sicherstellen, dass alles bereit ist...(dies ist zwar nicht erforderlich, aber wenn Sie meinen ...)
*****************************************************.
Der DRBL-Server ist bereit! Stellen Sie nun die Clients so ein, dass sie von PXE starten. (siehe http://drbl.org für weitere Einzelheiten)
P.S. Die Konfiguration ist unter /etc/drbl/drblpush.conf gespeichert. Wenn Sie also drblpush mit der selben Konfiguration nochmals laufen lassen wollen, können Sie dies mit: drblpush -c /etc/drbl/drblpush.conf tun
````



