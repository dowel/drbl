# Kubuntu Client

Installieren und konfigurieren. Beim KvFG Client ist u.a. das da an Bord:

````
sudo apt install kubuntu-restricted-extras kubuntu-restricted-addons arandr gimp gimp-gmic gimp-gap gimp-dcraw gimp-gutenprint gimp-help-de gimp-normalmap nextcloud-desktop vlc vlc-data vlc-plugin-access-extra libreoffice-help-de libreoffice-style-sifr libreoffice-l10n-de git vim zenity cifs-utils keepassxc inkscape youtube-dl flatpak plasma-discover-flatpak-backend flac p7zip-full p7zip-rar libdvdcss2 freeplane exfat-fuse exfat-utils libreoffice libreoffice-core fonts-crosextra-carlito fonts-crosextra-caladea digikam okular gwenview language-pack-gnome-de language-pack-kde-de language-pack-de-base

sudo dpkg-reconfigure libdvd-pkg

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

````
Aus dem Flathub können dann XMind und Xournal++ installiert werden ... oder was auch immer gewünscht ist.

# Hinweise zum Umgang mit dem DRBL-Server

## Image 2 Clonezilla-Server

## Einfacher Start, restliche Einstellungen am Client vornehmen:

    drbl-ocs -b -l de_DE.UTF-8 -p choose select_in_client

## Server danach anhalten

    drbl-ocs stop

--------------------------------------

## Autodeploy

### Kubuntu 20.04.x LTS (Clonezilla Server KvFG)

#### If Switches support multicast_restore

    drbl-ocs -g auto -e1 auto -e2 -r -x -j2 -p choose --clients-to-wait 156 --max-time-to-wait 15 -l de_DE.UTF-8 startdisk multicast_restore KUBUNTU sda

#### and if they don't support multicast_restore? We use

    drbl-ocs -g auto -e1 auto -e2 -r -x -j2 -ns -p reboot --clients-to-wait 1 --max-time-to-wait 15 -l de_DE.UTF-8 startdisk restore KUBUNTU sda

## Server anhalten

    drbl-ocs stop

--------------------------------------

## Start Clonezilla Server (Erstell- und Testmodus)

Im Prinzip ginge das auch so:

    /usr/sbin/dcs

Auswahl:

- all clients
- clonezilla start
- Beginner
- select in client
- Option überspringen für Standardmode
- choose at prompt

aber Autodeploy ist komfortabler.