# DRBL

Erst mal nur Handzettel, um nicht zu vergessen!

Eine ausführlichere Beschreibung der beinahe unveränderten Scripte, Links und Infos ist hier zu finden:

https://codeberg.org/dowel/installbox

Wesentliche Hinweise zur Bedienung in der folgenden Datei:

    hinweise.md

Installationshinweise für die Clonezilla / DRBL Serverseite hier:

    installdrbl.md

## Beachte

Updates des DRBL-Servers führen ganz flott dazu, dass Du den Setup-Prozess erneut durchführen darfst. Die Fehlermeldungen von DRBL sind dabei "nicht immer" hilfreich - z.B. tauchen dann so Dinge auf

    read image_hdr error=0

die aber (entgegen den Ergebnissen einer Internetrecherche) nicht auf kaputte Hardware hindeuten, sondern auf die Notwendigkeit eines erneuten Setups.

